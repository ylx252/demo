package com.demo.file;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestUpload {
	private static final String SERVER = "http://localhost/upload/index";
	private static final Logger LOGGER = Logger.getLogger("org.baeldung.httpclient.HttpClientMultipartTest");
	private CloseableHttpClient client;
	private HttpPost post;
	private CloseableHttpResponse response;
	String aaPath = "C:/Users/Administrator/Desktop/重点人员管控首页/重点人口系统管控 -基层民警首页.jpg";
	String bbPath = "C:/Users/Administrator/Desktop/重点人员管控首页/重点人员信息概况—领导层首页.jpg";

	@Test
	public void upload() throws Exception {
		String message = "This is a multipart post";
		// maxFormContentSize
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		builder.addBinaryBody("file", bytes(aaPath), ContentType.DEFAULT_BINARY, "aa.jpg");
		builder.addBinaryBody("file", bytes(bbPath), ContentType.DEFAULT_BINARY, "bb.jpg");
		builder.addBinaryBody("file", bytes(bbPath), ContentType.DEFAULT_BINARY, "cc.jpg");
		builder.addBinaryBody("file", bytes(bbPath), ContentType.DEFAULT_BINARY, "dd.jpg");
		builder.addBinaryBody("file", bytes(bbPath), ContentType.DEFAULT_BINARY, "ee.jpg");
		builder.addTextBody("text", message, ContentType.TEXT_PLAIN);
		HttpEntity entity = builder.build();
		post.setEntity(entity);
		response = client.execute(post);
		System.out.println(response.getStatusLine().getStatusCode());
	}

	byte[] bytes(String pathname) throws Exception {
		return IOUtils.toByteArray(new FileInputStream(new File(pathname)));
	}

	@Before
	public void before() {
		client = HttpClientBuilder.create().build();
		post = new HttpPost(SERVER);
	}

	void closeQuietly(Closeable closeable) {
		try {
			if (closeable != null) closeable.close();
		} catch (IOException e1) {
			LOGGER.log(Level.SEVERE, e1.getMessage(), e1);
		}
	}

	@After
	public void after() throws IllegalStateException, IOException {
		post.completed();
		IOUtils.closeQuietly(client);
		try {
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				IOUtils.closeQuietly(entity.getContent());
			}
		} finally {
			IOUtils.closeQuietly(response);
		}
	}
}
