package com.demo.ctrl;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("/upload")
@Controller
public class UploadCtrl {

	@RequestMapping("/index")
	@ResponseBody
	public Object index(@RequestParam("file") MultipartFile[] files, @RequestParam("text") String text) throws Exception {
		String parent = "C:/Users/Administrator/Desktop/aa";
		for (MultipartFile mf : files) {
			FileUtils.copyInputStreamToFile(mf.getInputStream(), new File(parent, mf.getOriginalFilename()));
		}
		System.out.println(text);
		return "aaa";
	}
}
