<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Uploadify上传</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/uploadify/uploadify.css" type="text/css"></link>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/uploadify/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#file_upload").uploadify({
	    	'height': 27, 
	    	'width': 80,  
	    	'buttonText': '浏 览',
	        'swf': '${pageContext.request.contextPath}/js/uploadify/uploadify.swf',
	        // 解决 session 丢失
	        'uploader': '${pageContext.request.contextPath}/u/uploadify',
			'auto' : false,
			'fileTypeExts' : '*.xls',
			'formData' : {
				'userName' : '',
				'qq' : ''
			},
			'onUploadStart' : function(file) {
				/*
					$("#file_upload").uploadify("settings",
						"formData", {
							'userName' : name,
							'qq' : qq
					});
				*/
				//$("#file_upload").uploadify("settings", "qq", );
			}
		});
	});
</script>
</head>

<body>
	<input type="file" name="uploadify" id="file_upload" />
	<hr>
	<a href="javascript:$('#file_upload').uploadify('upload','*')">开始上传</a>&nbsp;
	<a href="javascript:$('#file_upload').uploadify('cancel', '*')">取消所有上传</a>
</body>
</html>
