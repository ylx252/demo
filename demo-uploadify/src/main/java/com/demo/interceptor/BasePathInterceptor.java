package com.demo.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class BasePathInterceptor extends HandlerInterceptorAdapter {
	protected String basePathName = "BASE_PATH";
	protected String basePath;

	public void setBasePathName(String basePathName) {
		this.basePathName = basePathName;
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		request.setAttribute(basePathName, getBasePath(request));
		return true;
	}

	protected String getBasePath(HttpServletRequest request) {
		if (basePath == null) {
			String path = request.getContextPath();
			basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		}
		return basePath;
	}
}
