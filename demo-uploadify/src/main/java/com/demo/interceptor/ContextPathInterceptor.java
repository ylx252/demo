package com.demo.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ContextPathInterceptor extends HandlerInterceptorAdapter {

	protected String contextPathName = "CONTEXT_PATH";

	public void setContextPathName(String contextPathName) {
		this.contextPathName = contextPathName;
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		request.setAttribute(contextPathName, request.getContextPath());
		return true;
	}
}
