package com.demo.model;

import java.util.Date;

public class Blog {
	private Integer id;
	private String title;
	private String content;
	private Date publishTime;

	public Blog() {
	}

	public Blog(String title, String content, Date publishTime) {
		this.title = title;
		this.content = content;
		this.publishTime = publishTime;
	}

	public Blog(Integer id, String title, String content, Date publishTime) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.publishTime = publishTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	@Override
	public String toString() {
		return "Blog [id=" + id + ", title=" + title + ", content=" + content + ", publishTime=" + publishTime + "]";
	}
}
