package com.demo.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.demo.dao.IBlogDao;
import com.demo.model.Blog;
import com.ibatis.sqlmap.client.SqlMapClient;

@Repository
public class BlogDaoImpl  implements IBlogDao {
	@Autowired
	SqlMapClient sqlMapClient;

	@Override
	public Blog findById(Integer id) {
		try {
			return (Blog) sqlMapClient.queryForObject("findById", id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Blog> findAll() {
		try {
			return sqlMapClient.queryForList("findAll");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Blog> findByTitle(Blog b) {
		try {
			return sqlMapClient.queryForList("findByTitle", b);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void add(Blog b) {
		try {
			sqlMapClient.insert("add", b);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void deleteById(Integer id) {
		try {
			sqlMapClient.delete("deleteById", id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void update(Blog b) {
		try {
			sqlMapClient.update("update", b);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
