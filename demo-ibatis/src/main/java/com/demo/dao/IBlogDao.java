package com.demo.dao;

import java.util.List;

import com.demo.model.Blog;

public interface IBlogDao {
	Blog findById(Integer id);

	List<Blog> findAll();

	List<Blog> findByTitle(Blog b);

	void add(Blog b);

	void deleteById(Integer id);

	void update(Blog b);
}
