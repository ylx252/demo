package com.demo.test;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.demo.dao.IBlogDao;
import com.demo.model.Blog;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-ibatis.xml" })
public class TestBlog {
	@Autowired
	IBlogDao dao;

	@Test
	public void testAdd() {
		Blog b = new Blog();
		b.setTitle("ibatis title");
		b.setContent("ibatis content");
		b.setPublishTime(new Date());
		dao.add(b);
		System.out.println("add success");
	}

	@Test
	public void testDel() {
		dao.deleteById(12);
		System.out.println("del success");
	}

	@Test
	public void testFind() {
		Object data = null;
		data = dao.findById(14);
		System.out.println("data:" + data);
		System.out.println("findById success");

//		Blog b = new Blog();
//		b.setTitle("标题");
//		data = dao.findByTitle(b);
//		System.out.println("data:" + data);
//		System.out.println("findByTitle success");
	}

	@Test
	public void testUpd() {
		Blog b = dao.findById(14);
		b.setContent("update content1");
		dao.update(b);
		System.out.println("testUpd success");
	}
}
