package com.demo.tcpip;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientTest {
	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket s = null;
		try {
			s = new Socket("127.0.0.1", 6666);
			// 请求数据
			IoUtils.writeUTF(s.getOutputStream(), "haha.test");
			// 响应数据
			System.out.println("return:" + IoUtils.readUTF(s.getInputStream()));
		} finally {
			IoUtils.close(s);
		}
	}

}
