package com.demo.tcpip;

import java.net.ServerSocket;
import java.net.Socket;

public class ServerTest {
	public static void main(String[] args) throws Exception {
		ServerSocket ss = new ServerSocket(6666);
		Socket s = null;
		while (true) {
			try {
				s = ss.accept();
				String data = IoUtils.readUTF(s.getInputStream());
				// 打印客户端请求数据
				System.out.println("readUTF:" + data);
				// 响应数据到客户端
				IoUtils.writeUTF(s.getOutputStream(), "server:" + data);
			} finally {
				IoUtils.close(s);
			}
		}
	}
}
