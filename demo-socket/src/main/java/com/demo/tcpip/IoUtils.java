package com.demo.tcpip;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public abstract class IoUtils {

	public static final void close(Socket c) {
		if (c != null) {
			try {
				c.close();
			} catch (IOException e) {
				// ignore IOException
			}
		}
	}

	public static final void close(Closeable c) {
		if (c != null) {
			try {
				c.close();
			} catch (IOException e) {
				// ignore IOException
			}
		}
	}

	public static final void flush(Flushable f) {
		if (f != null) {
			try {
				f.flush();
			} catch (IOException e) {
				// ignore IOException
			}
		}
	}

	public static final String readUTF(InputStream in) throws IOException {
		DataInputStream input = null;
		try {
			input = new DataInputStream(in);
		} finally {
//			IoUtils.close(input);
		}
		return input.readUTF();
	}

	public static final void writeUTF(OutputStream out, String str) throws IOException {
		DataOutputStream output = null;
		try {
			output = new DataOutputStream(out);
			output.writeUTF(str);
			IoUtils.flush(output);
		} finally {
//			IoUtils.close(output);
		}
	}
}
