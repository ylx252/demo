package com.demo.mapper;

import java.util.List;

import com.demo.domain.Blog;

public interface BlogMapper {
	List<Blog> selectAll();
}