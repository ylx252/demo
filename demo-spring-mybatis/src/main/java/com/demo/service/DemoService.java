package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.domain.Blog;
import com.demo.mapper.BlogMapper;

@Service
@Transactional(readOnly = true)
public class DemoService {

	@Autowired
	BlogMapper blogMapper;

	@Transactional(readOnly = false)
	public List<Blog> findAll() {
		return blogMapper.selectAll();
	}
}
