package tk.mybatis.hot;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;

public class SqlSessionFactoryBuilder extends org.apache.ibatis.session.SqlSessionFactoryBuilder {

	@Override
	public SqlSessionFactory build(Configuration config) {
		HotConfiguration hotConfiguration = HotConfiguration.newInstance(config);
		return super.build(hotConfiguration);
	}
}
