package tk.mybatis.util;

public class FieldsException extends RuntimeException {

	private static final long serialVersionUID = -5920009828100581390L;

	public FieldsException() {
		super();
	}

	public FieldsException(String message, Throwable cause) {
		super(message, cause);
	}

	public FieldsException(String message) {
		super(message);
	}

	public FieldsException(Throwable cause) {
		super(cause);
	}

}
