package com.demo.test;

import tk.mybatis.util.Fields;

public class TestFields {
	public static void main(String[] args) {
		Class<?> type = A.class;
		Object object = new B();
		String name = "name";
		Fields.set(object, name, "王五", type);
		Object value = Fields.get(object, name, type);
		System.out.println(value);
	}

}

class A {
	String name = "张三";

}

class B extends A {
	String name = "李四";
	String email = "a@b.c";
}