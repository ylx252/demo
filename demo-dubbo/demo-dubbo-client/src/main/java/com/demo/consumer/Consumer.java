package com.demo.consumer;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demo.DemoService;

public class Consumer {

	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "classpath:consumer.xml" });
		context.start();

		DemoService demoService = (DemoService) context.getBean("demoService");
		String hello = demoService.sayHello("hejingyuan");
		System.out.println(hello);
		System.in.read();
	}

}