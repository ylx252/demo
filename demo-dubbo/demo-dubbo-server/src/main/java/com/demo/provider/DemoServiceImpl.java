package com.demo.provider;

import com.demo.DemoService;

public class DemoServiceImpl implements DemoService {

	@Override
	public String sayHello(String name) {
		return "hello " + name;
	}
}
