package com.cglib.core;

import com.cglib.beans.BeanCopier;
import com.cglib.beans.BeanMap;
import com.cglib.beans.BulkBean;
import com.cglib.beans.BulkBeanException;
import com.cglib.beans.FixedKeySet;
import com.cglib.proxy.Callback;
import com.cglib.proxy.Dispatcher;
import com.cglib.proxy.Factory;
import com.cglib.proxy.FixedValue;
import com.cglib.proxy.InvocationHandler;
import com.cglib.proxy.LazyLoader;
import com.cglib.proxy.MethodInterceptor;
import com.cglib.proxy.MethodProxy;
import com.cglib.proxy.Mixin;
import com.cglib.proxy.ProxyRefDispatcher;
import com.cglib.proxy.UndeclaredThrowableException;
import com.cglib.reflect.ConstructorDelegate;
import com.cglib.reflect.FastClass;
import com.cglib.reflect.MethodDelegate;
import com.cglib.reflect.MulticastDelegate;
import com.cglib.transform.impl.FieldProvider;
import com.cglib.transform.impl.InterceptFieldCallback;
import com.cglib.transform.impl.InterceptFieldEnabled;
import com.cglib.util.ParallelSorter;
import com.cglib.util.StringSwitcher;

public interface TypeName {

	String cglib_core_Converter = Converter.class.getName();
	String cglib_core_BeanCopier = BeanCopier.class.getName();
	String cglib_core_Signature = Signature.class.getName();
	String cglib_core_KeyFactory = KeyFactory.class.getName();
	String cglib_core_ReflectUtils = ReflectUtils.class.getName();

	String cglib_empty_Object = "com.cglib.empty.Object";

	String cglib_reflect_MulticastDelegate = MulticastDelegate.class.getName();
	String cglib_reflect_MethodDelegate = MethodDelegate.class.getName();
	String cglib_reflect_FastClass = FastClass.class.getName();
	String cglib_reflect_ConstructorDelegate = ConstructorDelegate.class.getName();

	String cglib_util_ParallelSorter = ParallelSorter.class.getName();
	String cglib_util_StringSwitcher = StringSwitcher.class.getName();

	String cglib_transform_impl_InterceptFieldCallback = InterceptFieldCallback.class.getName();
	String cglib_transform_impl_InterceptFieldEnabled = InterceptFieldEnabled.class.getName();
	String cglib_transform_impl_FieldProvider = FieldProvider.class.getName();

	String cglib_proxy_Dispatcher = Dispatcher.class.getName();
	String cglib_proxy_ProxyRefDispatcher = ProxyRefDispatcher.class.getName();
	String cglib_proxy_Factory = Factory.class.getName();
	String cglib_proxy_Callback = Callback.class.getName();
	String cglib_proxy_FixedValue = FixedValue.class.getName();
	String cglib_proxy_InvocationHandler = InvocationHandler.class.getName();
	String cglib_proxy_UndeclaredThrowableException = UndeclaredThrowableException.class.getName();
	String cglib_proxy_LazyLoader = LazyLoader.class.getName();
	String cglib_proxy_MethodProxy = MethodProxy.class.getName();
	String cglib_proxy_MethodInterceptor = MethodInterceptor.class.getName();
	String cglib_proxy_Mixin = Mixin.class.getName();

	String cglib_beans_BeanMap = BeanMap.class.getName();
	String cglib_beans_FixedKeySet = FixedKeySet.class.getName();
	String cglib_beans_BulkBean = BulkBean.class.getName();
	String cglib_beans_BulkBeanException = BulkBeanException.class.getName();
}
