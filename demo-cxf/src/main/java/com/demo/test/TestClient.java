package com.demo.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.demo.client.HelloService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-cxf-client.xml" })
public class TestClient {
	
	@Autowired
	HelloService hs;

	@Test
	public void t() {
		System.out.println(hs.sayHello("玄玉"));
	}

	//	wsdl2java用法：
	//	wsdl2java -p com -d src -all  aa.wsdl
	//	-p  指定其wsdl的命名空间，也就是要生成代码的包名:
	//	-d  指定要产生代码所在目录
	//	-client 生成客户端测试web service的代码
	//	-server 生成服务器启动web  service的代码
	//	-impl 生成web service的实现代码
	//	-ant  生成build.xml文件
	//	-all 生成所有开始端点代码：types,service proxy,,service interface, server mainline, client mainline, implementation object, and an Ant build.xml file.

	//	wsdl2java -p com.demo.client -d d://src -client  http://localhost/services/myHello?wsdl  
}
