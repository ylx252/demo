package com.demo.interceptor;

import java.util.List;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Node;

public class LicenseInInterceptor extends AbstractSoapInterceptor {
	public LicenseInInterceptor() {
		super(Phase.INVOKE);
	}

	@Override
	public void handleMessage(SoapMessage message) throws Fault {
		List<Header> headers = message.getHeaders();
		Object obj = null;
		for (Header header : headers) {
			if (header.getName().getLocalPart().equals("licenseInfo")) {
				obj = header.getObject();
				if (obj instanceof Node) {
					System.out.println("Receive the licenseInfo=[" + ((Node) obj).getTextContent() + "]");
				}
			}
		}
	}
}