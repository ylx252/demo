package com.demo.interceptor;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.databinding.DataBinding;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.phase.Phase;
import org.springframework.stereotype.Component;

@Component
public class LicenseOutInterceptor extends AbstractSoapInterceptor {
	public LicenseOutInterceptor() {
		super(Phase.WRITE);
	}

	@Override
	public void handleMessage(SoapMessage message) throws Fault {
		QName qname = new QName("http://blog.csdn.net/jadyer", "licenseInfo", "ns");
		DataBinding dataBinding = null;
		try {
			dataBinding = new JAXBDataBinding(String.class);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		Header header = new Header(qname, "Jadyer", dataBinding);
		message.getHeaders().add(header);
	}
}