package com.demo.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(targetNamespace = "http://blog.csdn.net/jadyer")
public interface HelloService {
	@WebMethod
	@WebResult(name = "sayHelloResult")
	public String sayHello(@WebParam(name = "name") String name);
}