package com.demo.service.impl;

import javax.jws.WebService;

import com.demo.service.HelloService;

@WebService(endpointInterface = "com.demo.service.HelloService", targetNamespace = "http://blog.csdn.net/jadyer")
public class HelloServiceImpl implements HelloService {
	@Override
	public String sayHello(String name) {
		System.out.println("Receive the name=[" + name + "]");
		if (null == name) {
			return "Hello,World";
		} else {
			return "Hello," + name;
		}
	}
}