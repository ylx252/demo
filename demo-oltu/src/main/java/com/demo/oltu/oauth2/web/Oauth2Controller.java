package com.demo.oltu.oauth2.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.oltu.oauth2.as.request.OAuthTokenRequest;

@RequestMapping("/oauth2")
@RestController
public class Oauth2Controller {

	@GetMapping("tokenEndpoint")
	public Object tokenEndpoint(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,
			OAuthSystemException {
		OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

		try {
			request = new HttpServletRequestWrapper(request) {

				@Override
				public String getParameter(String name) {
					if (OAuth.OAUTH_USERNAME.equals(name)) {//
						return "admin";
					} else if (OAuth.OAUTH_PASSWORD.equals(name)) {//
						return "123456";
					}
					return super.getParameter(name);
				}
			};
			// some code
			String accessToken = oauthIssuerImpl.accessToken();
			String refreshToken = oauthIssuerImpl.refreshToken();
			OAuthTokenRequest oauthRequest = new OAuthTokenRequest(request);

			String authzCode = oauthRequest.getCode();

			// some code
			OAuthResponse r = OAuthASResponse.tokenResponse(HttpServletResponse.SC_OK).setAccessToken(accessToken)
					.setExpiresIn("3600").buildJSONMessage();

			response.setStatus(r.getResponseStatus());
			return r.getBody();
			//if something goes wrong
		} catch (OAuthProblemException ex) {
			OAuthResponse r = OAuthResponse.errorResponse(401).error(ex).buildJSONMessage();

			response.setStatus(r.getResponseStatus());

			//			response.sendError(401);
			return r.getBody();
		}
	}

	/**
	 * 请求用户授权Token
	 * GET/POST
	 */
	@RequestMapping(value = "authorize", method = { RequestMethod.GET, RequestMethod.POST })
	public Object authorize() {
		return null;
	}

	/**
	 * 获取授权过的Access Token
	 * POST
	 */
	@PostMapping("access_token")
	public Object accessToken() {
		return null;
	}

	/**
	 * 授权信息查询接口
	 * POST
	 */
	@PostMapping("get_token_info")
	public Object getTokenInfo() {
		return null;
	}

	/**
	 * 授权回收接口
	 * GET/POST
	 */
	@RequestMapping(value = "revokeoauth2", method = { RequestMethod.GET, RequestMethod.POST })
	public Object revokeoauth2() {
		return null;
	}
}
