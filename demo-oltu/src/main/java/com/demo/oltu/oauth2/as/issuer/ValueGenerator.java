package com.demo.oltu.oauth2.as.issuer;

public interface ValueGenerator {
	public String generateValue();

	public String generateValue(String param);
}