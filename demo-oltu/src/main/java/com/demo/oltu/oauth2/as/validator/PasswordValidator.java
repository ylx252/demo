package com.demo.oltu.oauth2.as.validator;

import javax.servlet.http.HttpServletRequest;

import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.validators.AbstractValidator;

public class PasswordValidator extends AbstractValidator<HttpServletRequest> {

	public PasswordValidator() {
		requiredParams.add(OAuth.OAUTH_USERNAME);
		requiredParams.add(OAuth.OAUTH_PASSWORD);

		enforceClientAuthentication = true;
	}

	@Override
	public void validateMethod(HttpServletRequest request) throws OAuthProblemException {
		// skip validate
	}

	@Override
	public void validateContentType(HttpServletRequest request) throws OAuthProblemException {
		// skip validate
	}

	@Override
	public void validateClientAuthenticationCredentials(HttpServletRequest request) throws OAuthProblemException {
		// skip validate
	}

}
