package com.demo.oltu.oauth2.as.request;

import javax.servlet.http.HttpServletRequest;

import org.apache.oltu.oauth2.as.request.AbstractOAuthTokenRequest;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;
import org.apache.oltu.oauth2.common.validators.OAuthValidator;

import com.demo.oltu.oauth2.as.validator.PasswordValidator;

public class OAuthTokenRequest extends AbstractOAuthTokenRequest {

	public OAuthTokenRequest(HttpServletRequest request) throws OAuthSystemException, OAuthProblemException {
		super(request);
	}

	@Override
	protected OAuthValidator<HttpServletRequest> initValidator() throws OAuthProblemException, OAuthSystemException {
		return OAuthUtils.instantiateClass(PasswordValidator.class);
	}

}
