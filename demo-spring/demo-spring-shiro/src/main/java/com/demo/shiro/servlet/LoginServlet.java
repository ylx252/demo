package com.demo.shiro.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.shiro.common.ShiroUtils;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("login " + req.getMethod());
		req.getRequestDispatcher("login.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			ShiroUtils.login(req.getParameter("username"), req.getParameter("password"));
			resp.sendRedirect("success.jsp");
		} catch (Exception e) {
			req.setAttribute("errorInfo", "登录名或密码错误。");
			req.getRequestDispatcher("login.jsp").forward(req, resp);
		}
	}

}
