package com.demo.shiro.test;

import org.apache.shiro.subject.Subject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.demo.shiro.common.ShiroUtils;

public class PermissionTest {

	@Test
	public void testRole() {
		Subject currentUser = ShiroUtils.login("admin", "123");
		System.out.println(currentUser.isPermitted("user:select"));
	}

	@After
	public void _a() {

	}

	@Before
	public void _b() {
		ShiroUtils.init("classpath:shiro_role");
	}
}
