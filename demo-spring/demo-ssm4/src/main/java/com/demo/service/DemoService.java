package com.demo.service;

import java.util.List;

import com.demo.domain.Blog;

public interface DemoService {
	List<Blog> findAll();
}
