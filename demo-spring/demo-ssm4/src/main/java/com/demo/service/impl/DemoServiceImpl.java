package com.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.domain.Blog;
import com.demo.mapper.BlogMapper;
import com.demo.service.DemoService;

@Service
@Transactional(readOnly = true)
public class DemoServiceImpl implements DemoService {

	@Autowired
	BlogMapper blogMapper;

	@Override
	public List<Blog> findAll() {
		return blogMapper.selectAll();
	}
}
