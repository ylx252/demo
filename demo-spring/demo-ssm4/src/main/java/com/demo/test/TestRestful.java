package com.demo.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.alibaba.fastjson.JSON;

@ContextConfiguration({
//
		"classpath:spring-context.xml"//
		, "classpath:spring-context-token.xml"//
		, "classpath:spring-context-jedis.xml"//
})
public class TestRestful extends AbstractSpringMvcJUnit4SpringContextTests {

	@Test
	public void testApiController_index() {
		MediaType mediaType = MediaType.parseMediaType("application/json;charset=UTF-8");
		try {
			RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/index").accept(mediaType);
			MvcResult result = this.mockMvc.perform(requestBuilder)//
					.andExpect(MockMvcResultMatchers.status().isOk())//
					.andExpect(MockMvcResultMatchers.content().contentType(mediaType))//
					.andExpect(MockMvcResultMatchers.jsonPath("$.result").value(true)).andReturn();
			MockHttpServletResponse response = result.getResponse();
			print(response.getContentAsString());
			print(response.getContentType());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void _a() {

	}

	@Before
	public void _b() {
		System.out.println(applicationContext);
		System.out.println(mockMvc);
	}

	@Override
	protected Object[] getControllers() {
		return null;
	}

	void print(Object object) {
		if (object == null) {
			System.out.println("null");
			return;
		}
		Class<?> clazz = object.getClass();
		if (clazz.isPrimitive()//
				|| Integer.class == clazz//
				|| Long.class == clazz//
				|| Double.class == clazz//
				|| Float.class == clazz//
				|| Character.class == clazz//
				|| String.class == clazz//
				|| CharSequence.class.isAssignableFrom(clazz)//
		) {
			System.out.println(object);
			return;
		}
		System.out.println(JSON.toJSONString(object));
	}
}
