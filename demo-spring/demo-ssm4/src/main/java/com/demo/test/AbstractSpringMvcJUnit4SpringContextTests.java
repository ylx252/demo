package com.demo.test;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.StandaloneMockMvcBuilder;

public abstract class AbstractSpringMvcJUnit4SpringContextTests extends AbstractJUnit4SpringContextTests implements
		InitializingBean {

	protected MockMvc mockMvc;

	public AbstractSpringMvcJUnit4SpringContextTests() {}

	@Override
	public void afterPropertiesSet() throws Exception {
		Object[] controllers = getControllers();
		StandaloneMockMvcBuilder builder = MockMvcBuilders.standaloneSetup(controllers);
		initStrategies(builder);
		this.mockMvc = builder.build();
	}

	protected abstract Object[] getControllers();

	protected void initStrategies(StandaloneMockMvcBuilder builder) {

	}
}
