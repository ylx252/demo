package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.demo.domain.Blog;
import com.demo.service.DemoService;

@RestController
@RequestMapping("/demo")
public class DemoController {

	@Autowired
	DemoService service;

	@RequestMapping("/findAll")
	public List<Blog> findAll() {
		return service.findAll();
	}
	
	@RequestMapping("welcome")
	public ModelAndView welcome(){
		return new ModelAndView("welcome");
	}
}
