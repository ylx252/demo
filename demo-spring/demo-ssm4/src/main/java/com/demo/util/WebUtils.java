package com.demo.util;

public abstract class WebUtils {
	public static final String REDIRECT = "redirect:";

	/**
	 * 重定向
	 */
	public static String redirect(String url) {
		return REDIRECT + url;
	}

}
