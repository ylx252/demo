package com.demo.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

/**
 * java.util.Properties 工具类
 */
public abstract class PropertiesUtils {

	public static final Properties loadProperties(String s) throws IOException {
		return PropertiesLoaderUtils.loadProperties(new ByteArrayResource(s.getBytes()));
	}

	public static final Properties loadProperties(InputStream is) throws IOException {
		return PropertiesLoaderUtils.loadProperties(new InputStreamResource(is));
	}
}
