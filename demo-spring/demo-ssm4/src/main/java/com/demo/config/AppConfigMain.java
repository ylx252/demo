package com.demo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

@Configuration
@ComponentScan(value = { "com.demo" }, excludeFilters = { @Filter({ Controller.class, RestController.class }) })
public class AppConfigMain {

}
