package com.demo.mapper;

import com.demo.domain.Blog;

import tk.mybatis.mapper.common.Mapper;

public interface BlogMapper extends Mapper<Blog> {
	
}