package tk.mybatis.mapper.provider;

import org.apache.ibatis.mapping.MappedStatement;

import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;

public class DynamicSQLProvider extends MapperTemplate {

	public DynamicSQLProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
		super(mapperClass, mapperHelper);
	}

	public String insert(MappedStatement ms) {
		return "${sql}";
	}

	public String update(MappedStatement ms) {
		return "${sql}";
	}

	public String delete(MappedStatement ms) {
		return "${sql}";
	}

	public String selectOne(MappedStatement ms) {
		return "${sql}";
	}

	public String selectList(MappedStatement ms) {
		return "${sql}";
	}
}
