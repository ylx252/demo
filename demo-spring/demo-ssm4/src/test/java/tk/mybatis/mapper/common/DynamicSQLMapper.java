package tk.mybatis.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import tk.mybatis.mapper.provider.DynamicSQLProvider;

public interface DynamicSQLMapper<T> {

	@InsertProvider(type = DynamicSQLProvider.class, method = "dynamicSQL")
	int insert(@Param("sql") String sql, @Param("param") Object param);

	@UpdateProvider(type = DynamicSQLProvider.class, method = "dynamicSQL")
	int update(@Param("sql") String sql, @Param("param") Object param);

	@DeleteProvider(type = DynamicSQLProvider.class, method = "dynamicSQL")
	int delete(@Param("sql") String sql, @Param("param") Object param);

	@SelectProvider(type = DynamicSQLProvider.class, method = "dynamicSQL")
	T selectOne(@Param("sql") String sql, @Param("param") Object param);

	@SelectProvider(type = DynamicSQLProvider.class, method = "dynamicSQL")
	List<T> selectList(@Param("sql") String sql, @Param("param") Object param);
}
