package com.demo.spring.data.elasticsearch;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

public interface SampleElasticsearchRepository extends ElasticsearchCrudRepository<SampleEntity, String> {

}
