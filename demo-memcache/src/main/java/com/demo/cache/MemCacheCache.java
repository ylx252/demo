package com.demo.cache;

import java.util.HashSet;
import java.util.Set;

import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.util.Assert;

import com.danga.MemCached.MemCachedClient;

@SuppressWarnings("unchecked")
public class MemCacheCache implements Cache {

	private MemCachedClient client;
	private static final String CACHE_NAMES = "cache_names";
	private String name;

	public MemCacheCache() {

	}

	public MemCacheCache(String name, MemCachedClient client) {
		Assert.notNull(client, "MemCachedClient must not be null");
		this.client = client;
		this.name = name;
	}

	public void setClient(MemCachedClient client) {
		this.client = client;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Object getNativeCache() {
		return this.client;
	}

	@Override
	public ValueWrapper get(Object key) {
		Object value = this.client.get(objectToString(key));
		return (value != null ? new SimpleValueWrapper(value) : null);
	}

	@Override
	public void put(Object key, Object value) {
		String name = objectToString(key);
		this.client.set(name, value);
		this.addCacheName(name);
	}

	@Override
	public void evict(Object key) {
		String name = objectToString(key);
		this.client.delete(name);
		this.removeCacheName(name);
	}

	@Override
	public void clear() {
		Set<String> cacheNames = this.getCacheNames();
		for (String name : cacheNames) {
			this.equals(objectToString(name));
		}
	}

	@Override
	public <T> T get(Object key, Class<T> type) {
		return (T) client.get(objectToString(key));
	}

	@Override
	public ValueWrapper putIfAbsent(Object key, Object value) {
		ValueWrapper valueWrapper = get(key);
		return valueWrapper != null ? valueWrapper : new SimpleValueWrapper(value);
	}

	void addCacheName(String key) {
		getCacheNames().add(key);
	}

	Set<String> getCacheNames() {
		if (!this.client.keyExists(CACHE_NAMES)) {
			this.put(CACHE_NAMES, new HashSet<String>());
		}
		return get(CACHE_NAMES, HashSet.class);
	}

	void removeCacheName(String key) {
		getCacheNames().remove(key);
	}

	static String objectToString(Object object) {
		if (object == null) {
			return null;
		} else if (object instanceof String) {
			return (String) object;
		} else {
			return object.toString();
		}
	}
}
