package com.demo.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;
import org.springframework.util.Assert;

public class MemCacheCacheManager extends AbstractCacheManager {

	private List<Cache> caches = new ArrayList<Cache>();
	private MemCacheCache memCacheCache;

	public MemCacheCacheManager() {

	}

	public MemCacheCacheManager(MemCacheCache memCacheCache) {
		this.memCacheCache = memCacheCache;
	}

	@Override
	protected List<? extends Cache> loadCaches() {
		Collection<String> cacheNames = super.getCacheNames();
		for (String cacheName : cacheNames) {
			this.caches.add(getCache(cacheName));
		}
		return this.caches;
	}

	public void setCaches(List<Cache> caches) {
		this.caches = caches;
	}

	public void setMemCacheCache(MemCacheCache memCacheCache) {
		this.memCacheCache = memCacheCache;
		super.addCache(memCacheCache);
	}

	public Cache getCache(String name) {
		checkState();
		Cache cache = super.getCache(name);
		if (cache == null) {
			addCache(memCacheCache);
		}
		return cache;
	}

	private void checkState() {
		Assert.notNull(memCacheCache, "MemCacheCache must not be null.");
	}
}