package com.demo.test;

import java.io.Serializable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.danga.MemCached.MemCachedClient;
import com.danga.MemCached.SockIOPool;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:spring-context*.xml" })
public class TestCache {

	@Autowired
	MemCachedClient client;

	@Autowired
	SockIOPool pool;

	@Test
	public void t() {
		Bean bean = new Bean();
		bean.setAge(21);
		bean.setName("名字");
		

		client.set("bean", bean);
		bean.setAge(22);
		client.replace("bean", bean);
		Bean b = (Bean) client.get("bean");
		System.out.println("bean  toString ：" + b.toString());
	}

	public static class Bean implements Serializable {
		private static final long serialVersionUID = -4811925039847117703L;

		private String name;

		private int age;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public String toString() {
			String bean = "{name:" + this.getName() + ",age:" + this.getAge() + "}";
			return bean;
		}
	}
}
