package org.mybatis.generator.config.xml;

import java.util.Enumeration;
import java.util.Properties;

import org.mybatis.generator.config.CommentGeneratorConfiguration;
import org.mybatis.generator.config.JDBCConnectionConfiguration;
import org.mybatis.generator.config.JavaClientGeneratorConfiguration;
import org.mybatis.generator.config.JavaModelGeneratorConfiguration;
import org.mybatis.generator.config.JavaTypeResolverConfiguration;
import org.mybatis.generator.config.SqlMapGeneratorConfiguration;
import org.mybatis.generator.plugins.SerializablePlugin;

public class MyBatisGeneratorRun {

	private static void applyPlugins(ContextConfiguration context) {
		context.addPluginConfiguration(SerializablePlugin.class.getName());
	}

	// 初始化配置
	private static void applyProperties(GeneratorConfiguration configuration) {
		configuration.addProperty("resource", "jdbc.properties");
		//		configuration.addProperty("url", "");
		configuration.addProperty("classPathEntry", "jdbc.properties");

		configuration.addProperty("context.id", "DB2Tables");
		configuration.addProperty("context.targetRuntime", "MyBatis3");
		configuration.addProperty("context.defaultModelType", "flat");
		//		configuration.addProperty("context.introspectedColumnImpl", "");
	}

	private static void applyClassPathEntry(GeneratorConfiguration configuration) {
		configuration.addProperty("location", "${projectPath}/${classPathEntry}");

	}

	private static void applyCommentGenerator(ContextConfiguration context) {
		CommentGeneratorConfiguration commentGeneratorConfiguration = new CommentGeneratorConfiguration();
		commentGeneratorConfiguration.addProperty("suppressAllComments", "true");
		commentGeneratorConfiguration.addProperty("suppressDate", "true");
		context.setCommentGeneratorConfiguration(commentGeneratorConfiguration);
	}

	private static void applyJdbcConnection(ContextConfiguration context) {
		JDBCConnectionConfiguration jdbcConnectionConfiguration = new JDBCConnectionConfiguration();
		jdbcConnectionConfiguration
				.setConnectionURL("jdbc:mysql://120.26.45.21:3306/qpwjs?useUnicode=true&amp;characterEncoding=utf-8");
		jdbcConnectionConfiguration.setDriverClass("com.mysql.jdbc.Driver");
		jdbcConnectionConfiguration.setPassword("root");
		jdbcConnectionConfiguration.setUserId("root");
		context.setJdbcConnectionConfiguration(jdbcConnectionConfiguration);
	}

	private static void applyJavaTypeResolver(ContextConfiguration context) {
		JavaTypeResolverConfiguration javaTypeResolverConfiguration = new JavaTypeResolverConfiguration();
		//		javaTypeResolverConfiguration.setConfigurationType(configurationType);
		javaTypeResolverConfiguration.addProperty("forceBigDecimals", "false");
		context.setJavaTypeResolverConfiguration(javaTypeResolverConfiguration);
	}

	private static void applyJavaModelGenerator(ContextConfiguration context) {
		JavaModelGeneratorConfiguration javaModelGeneratorConfiguration = new JavaModelGeneratorConfiguration();
		javaModelGeneratorConfiguration.setTargetPackage("${package}.entity");
		javaModelGeneratorConfiguration.setTargetProject("${projectPath}/${targetProject}");
		javaModelGeneratorConfiguration.addProperty("enableSubPackages", "true");
		javaModelGeneratorConfiguration.addProperty("trimStrings", "true");
		context.setJavaModelGeneratorConfiguration(javaModelGeneratorConfiguration);
	}

	private static void applySqlMapGenerator(ContextConfiguration context) {
		SqlMapGeneratorConfiguration sqlMapGeneratorConfiguration = new SqlMapGeneratorConfiguration();
		sqlMapGeneratorConfiguration.setTargetPackage("${package}.mapping");
		sqlMapGeneratorConfiguration.setTargetProject("${projectPath}/${targetProject}");
		sqlMapGeneratorConfiguration.addProperty("enableSubPackages", "true");
		context.setSqlMapGeneratorConfiguration(sqlMapGeneratorConfiguration);
	}

	private static void applyJavaClientGenerator(ContextConfiguration context) {
		JavaClientGeneratorConfiguration javaClientGeneratorConfiguration = new JavaClientGeneratorConfiguration();
		javaClientGeneratorConfiguration.setConfigurationType("XMLMAPPER");
		javaClientGeneratorConfiguration.setTargetPackage("${package}.dao");
		javaClientGeneratorConfiguration.setTargetProject("${projectPath}/${targetProject}");
		javaClientGeneratorConfiguration.addProperty("enableSubPackages", "true");
		context.setJavaClientGeneratorConfiguration(javaClientGeneratorConfiguration);
	}

	private static void applyTables(ContextConfiguration context, String tableName, String domainObjectName) {
		context.addTableConfiguration(tableName, domainObjectName);
	}

	public static void main(String[] args) {
		GeneratorConfiguration configuration = new GeneratorConfiguration();

		ContextConfiguration context = new ContextConfiguration();
		context.setTargetRuntime("MyBatis3");
		context.setDefaultModelType("flat");

		applyProperties(configuration);
		applyClassPathEntry(configuration);
		applyPlugins(context);
		applyCommentGenerator(context);
		applyJdbcConnection(context);
		applyJavaTypeResolver(context);
		applyJavaModelGenerator(context);
		applySqlMapGenerator(context);
		applyJavaClientGenerator(context);
		Properties tables = getTables(null);

		Enumeration<?> propertyNames = tables.propertyNames();
		while (propertyNames.hasMoreElements()) {
			String propertyName = (String) propertyNames.nextElement();
			applyTables(context, propertyName, tables.getProperty(propertyName));
		}
		configuration.addContext(context.genContext());

		System.out.println(configuration.genConfiguration());
		//
		//		try {
		//			List<String> warnings = new ArrayList<String>();
		//			ConfigurationParser cp = new ConfigurationParser(warnings);
		//			Configuration config = cp.parseConfiguration(configuration.getInputStream());
		//			DefaultShellCallback callback = new DefaultShellCallback(true);
		//			MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
		//			myBatisGenerator.generate(null);
		//			for (String warning : warnings) {
		//				System.out.println(warning);
		//			}
		//			System.out.println("success:");
		//		} catch (Exception e) {
		//			System.out.println("error:");
		//			e.printStackTrace();
		//		}
	}

	public static final Properties getTables(String type) {
		Properties tables = new Properties();
		tables.setProperty("t_local_linecompany", "LocalLinecompany");
		tables.setProperty("t_local_company", "LocalCompany");
		tables.setProperty("t_local_line", "LocalLine");
		return tables;
	}
}
