package org.apache.ibatis.plugin;

import org.apache.ibatis.binding.BindingException;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.Configuration;

/**
 * 拦截器对象
 */
public class Invocations {
	private final Invocation invocation;

	private final MappedStatement mappedStatement;
	private final Configuration configuration;

	private Invocations(Invocation invocation) {
		this.invocation = invocation;
		this.mappedStatement = MybatisFactory.doResolveMappedStatement(invocation);
		this.configuration = mappedStatement != null ? mappedStatement.getConfiguration() : MybatisFactory
				.doResolveConfiguration(invocation);
	}

	/**
	 * 包装
	 */
	public static Invocations wrap(Invocation invocation) {
		return new Invocations(invocation);
	}

	public Invocation getInvocation() {
		return invocation;
	}

	public MappedStatement getMappedStatement() {
		if (mappedStatement == null) { //
			throw new BindingException("当前拦截对象不包含 MappedStatement." + invocation.getTarget());
		}
		return mappedStatement;
	}

	public Configuration getConfiguration() {
		if (configuration == null) { //
			throw new BindingException("当前拦截对象不包含 Configuration." + invocation.getTarget());
		}
		return configuration;
	}
}
