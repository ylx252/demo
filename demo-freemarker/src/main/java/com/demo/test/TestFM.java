package com.demo.test;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-freemarker.xml" })
public class TestFM {

	@Autowired
	TemplateBean tb;

	public static class Abc {
		private String user = "BigJoe";
		private String url = "www.baidu.com";
		private String name = "baidu";

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	@Test
	public void t() throws Exception {
		/*创建一个数据模型Createadatamodel*/
		Map<String, Object> root = new HashMap<String, Object>();
		root.put("user", "BigJoe");
		root.put("url", "www.baidu.com");
		root.put("name", "baidu");
//		String s = tb.process("Welcome.ftl", root);
		String s = tb.process("Welcome.ftl", new Abc());
		System.out.println(s);
	}

	public void t2() throws Exception {
		/* 在整个应用的生命周期中，这个工作你应该只做一次。 */
		/* 创建和调整配置。 */
		Configuration cfg = new Configuration(Configuration.getVersion());
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		stringLoader.putTemplate("myTemplate", "rule\n" + "<#if action??>\n" + " ${action}\n" + "</#if>\n"
				+ "<#if protocol??>\n" + " ${protocol}\n" + "</#if>\n" + "<#if source_ip_address??>\n"
				+ "\t<#if source_wildcard??>\n" + " source ${source_ip_address} ${source_wildcard}\n" + "\t</#if>\n"
				+ "</#if>\n" + "<#if destination_ip_address??>\n" + "\t<#if destination_wildcard??>\n"
				+ " destination ${destination_ip_address} ${destination_wildcard}\n" + "\t</#if>\n" + "</#if>\n"
				+ "<#if operator??>\n" + "\t<#if destination_port??>\n" + " destination_port ${operator} ${destination_port}\n"
				+ "\t</#if>\n" + "</#if>");
		cfg.setTemplateLoader(stringLoader);
		Template temp = cfg.getTemplate("myTemplate", "utf-8");
		/* 创建数据模型 */
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("action", "deny");
		root.put("protocol", "udp");
		root.put("source_ip_address", "192.168.56.123");
		root.put("source_wildcard", "0.0.0.255");
		root.put("destination_ip_address", "192.168.56.123");
		root.put("destination_wildcard", "0.0.0.255");
		root.put("operator", "gt");
		root.put("destination_port", "128");
		Writer out = new StringWriter(2048);
		temp.process(root, out);
		System.out.println(out.toString().replaceAll("[\\n\\r]", ""));
		out.flush();
	}
}
