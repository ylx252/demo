package com.demo.test;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class TemplateBean {
	Configuration configuration;

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public String process(String name) {
		return process(name, null);
	}

	public String process(String name, Object dataModel) {
		return process(name, dataModel, 2048);
	}

	public String process(String name, Object dataModel, int initialSize) {
		Writer out = new StringWriter(initialSize);
		try {
			Template template = configuration.getTemplate(name);
			template.process(dataModel, out);
			return out.toString().replaceAll("[\\n\\r]", "");
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				out.flush();
			} catch (IOException e) {
				// ignore
			}
		}
	}
}
