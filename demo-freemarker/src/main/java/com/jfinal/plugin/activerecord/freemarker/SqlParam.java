package com.jfinal.plugin.activerecord.freemarker;

public class SqlParam<V> {
	private String sql;
	private V[] params;

	public String getSql() {
		return sql;
	}

	public V[] getParams() {
		return params;
	}

	public static final class Builder<V> {
		SqlParam<V> sqlParam = new SqlParam<V>();

		public Builder<V> setSql(String sql) {
			sqlParam.sql = sql;
			return this;
		}

		public Builder<V> setParams(V[] params) {
			sqlParam.params = params;
			return this;
		}

		public SqlParam<V> build() {
			return sqlParam;
		}
	}

}
