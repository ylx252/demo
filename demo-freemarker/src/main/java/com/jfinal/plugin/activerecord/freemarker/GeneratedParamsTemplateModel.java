/**
 *    Copyright 2015 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.jfinal.plugin.activerecord.freemarker;

import freemarker.template.TemplateModel;

import java.util.List;

/**
 * Just a wrapper for list of generated params. Only to be able to return this object from
 * {@link freemarker.template.TemplateHashModel#get(java.lang.String)} method.
 *
 * @author elwood
 */
public class GeneratedParamsTemplateModel implements TemplateModel {
	private final List<Object> generatedParams;

	public GeneratedParamsTemplateModel(List<Object> generatedParams) {
		this.generatedParams = generatedParams;
	}

	public List<Object> getGeneratedParams() {
		return generatedParams;
	}
}
