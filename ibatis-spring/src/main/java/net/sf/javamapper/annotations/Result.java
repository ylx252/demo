package net.sf.javamapper.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Result {
	
	String column() default "";
	String jdbcType() default "";
	String nullValue() default "";
	Class<?> resultMap() default Class.class;
	Class<?> typeHandler() default Class.class;
	boolean groupBy() default false;
}
